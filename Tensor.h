#ifndef TENSOR_H
#define TENSOR_H
#include "Size.h"
#include <iostream>
#include <typeinfo>
#include <math.h>
#include <string.h>
#include <sstream>
#include <iomanip>
#include <vector>

using namespace std;

template<typename T = int>
class Tensor {
public:
	//constructor 
	Tensor(auto &arr, bool req_grad=false) {
		std::vector<int> dims;
		dims.push_back(sizeof(arr) / sizeof(arr[0]));
		while(1) {
			auto &temp = arr[0];
			dims.push_back(sizeof(temp) / sizeof(temp[0]));
			if (typeid(temp[0]) == typeid(T)){
				data = &temp[0];
				break;
			}
		}
		dim = Size(dims);
		requires_grad = req_grad;
	}
	//indexing getter
	auto operator[](const int index) const{
		return data[index];
	}
	//indexing setter
	auto& operator[](const int index){
		return data[index];
	}

	friend ostream & operator<<(ostream &out, Tensor<T> &t) {
		string s = "tensor([";
		int size = t.size(0)*t.size(1);
		std::ostringstream streamObj;
		streamObj << std::scientific;
		streamObj << std::setprecision(4);
		for (int i = 0; i < size - 1; i++){
			streamObj << t[i] << ", ";
			if (streamObj.str().size() >= 72) {
				s += streamObj.str() + "\n	";
				streamObj.str(std::string());
			}
		}
		streamObj << t[size - 1];
		s += streamObj.str() + "])";
		out << s;
	    return out;
	}
	//tensor-tensor multiplication
	Tensor<T> operator*(Tensor<T> &rhs) {
		Tensor<T> &lhs = *this;
		Tensor<T> out = lhs;
		int size = lhs.size();
		for (int i = 0; i < size; i++){
			out[i] = lhs[i] * rhs[i];
		}
		return out;
	}
	//size array
	Size& size() {
		return dim;
	}
	//index in the size array
	int size(int index) {
		return dim[index];
	}
	bool requires_grad = false;
private:
	T* data;
	Size dim;
	T *prev = 0;
};
#endif