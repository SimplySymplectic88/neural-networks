#include "Tensor.h"
#include <iostream>
#include <typeinfo>
#include <iterator>
#include <vector>

using namespace std;

int main(){
	float arr1[3][4] = {{1.3,   2.4,   3.5,   4.6},
					{5.7,   6.8,   7.9,   8.10},
					{9.11,  10.12, 11.13, 12.14}};
	Tensor<float> x = Tensor<float>(arr1);
	//vector<int> dim;
	//dim.push_back(2);
	//Size s = Size(dim);
	//std::cout << s << std::endl;
	std::cout << x << std::endl;
	//float arr2[] = {1.3,2.4,3.5,4.6,5.7,6.8};
	//Tensor<float> y = x*x;
	return 0;
}