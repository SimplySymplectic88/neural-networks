EXENAME = Tensor
OBJS = main.o Tensor.o Size.o
CC = g++
DEBUG = -g
CFLAGS = -std=c++17 -Wall -c $(DEBUG)
LFLAGS = -Wall $(DEBUG)

all : $(EXENAME)

$(EXENAME) : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o $(EXENAME)

main.o : main.cpp Tensor.h Size.h
	$(CC) $(CFLAGS) main.cpp

Tensor.o : Tensor.cpp Tensor.h Size.h
	$(CC) $(CFLAGS) Tensor.cpp

Size.o : Size.cpp Size.h
	$(CC) $(CFLAGS) Size.cpp
	
clean :
	-rm -f *.o $(EXENAME)
