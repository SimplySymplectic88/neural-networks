#ifndef SIZE_H
#define SIZE_H
#include <iostream>
#include <vector>

using namespace std;

class Size {
public:
	//blank constructor
	Size();
	//constructor 
	Size(vector<int> &vec);
	//indexing getter
	int operator[](const int index) const;
	//indexing setter
	int& operator[](const int index);
	int shape();
	friend std::ostream & operator<<(ostream &out, Size &d);
private:
	std::vector<int> size;
	int dim = 0;
};
#endif
