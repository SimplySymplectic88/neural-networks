#include "Size.h"
#include <iostream>
#include <sstream>

Size::Size() {
	size = std::vector<int>();
	dim = 0;
}

Size::Size(vector<int> &vec){
	size = vec;
	dim = vec.size();
}

int Size::operator[](const int index) const{
	return size[index];
}

int& Size::operator[](const int index){
	return size[index];
}

int Size::shape() {
	return dim;
}

std::ostream & operator<<(ostream &out, Size &d) {
	string s = "Size([";
	int size = d.shape();
	std::ostringstream streamObj;
	for (int i = 0; i < size - 1; i++){
			streamObj << d[i] << ", ";
	}
	streamObj << d[size - 1];
	s += streamObj.str() + "])";
	out << s;
    return out;
}



